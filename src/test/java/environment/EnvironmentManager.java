package environment;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class EnvironmentManager {

    public static void initWebDriver() {
    	System.setProperty("webdriver.gecko.driver","/Users/Scott.Morrison/Documents/workspace/selenium/FSPCA/lib/geckodriver");
        WebDriver driver = new FirefoxDriver();
        RunEnvironment.setWebDriver(driver);
    }

    public static void shutDownDriver() {
        RunEnvironment.getWebDriver().quit();
    }
}
