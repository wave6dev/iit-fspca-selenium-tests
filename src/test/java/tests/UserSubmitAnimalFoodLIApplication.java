package tests;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import environment.EnvironmentManager;
import environment.RunEnvironment;

public class UserSubmitAnimalFoodLIApplication {

	@Before
	public void startBrowser() {
		EnvironmentManager.initWebDriver();
	}
	
	@Test
    public void SubmitAnimalFoodLIApplication() {
    	
    	WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://full-fspca.cs25.force.com/FSPCA/s/");
        
        WebElement loginLink = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='header-overlay']/div/div[5]/div/div/div/div/a/span")));
        loginLink.click(); 
       
        
        WebElement usernameInput = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#sfdc_username_container .inputBox")));
        WebElement passwordInput = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#sfdc_password_container .inputBox")));
        
        usernameInput.sendKeys("amy.seibert@wave6.com");
        passwordInput.sendKeys("Success123");
        
        WebElement loginButton = driver.findElement(By.cssSelector(".loginButton"));
        loginButton.click(); 
        
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".slds-is-relative:nth-child(6) .slds-truncate")));
        element.click();
        
        WebElement newApplication = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//section/div/button")));
        newApplication.click();
        
        WebElement dialogContainer = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.cLI_Application")));
        assertNotNull(null, dialogContainer);

        
        WebElement humanFoodCheckbox = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.animalfoodcheckbox .slds-checkbox--faux")));
        assertNotNull(null, humanFoodCheckbox);
        
        humanFoodCheckbox.click();
        
        WebElement trainingExperience = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div/div/textarea")));
        trainingExperience.sendKeys("testing creating an li application");
        
        WebElement saveButton = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[5]/button")));
        saveButton.click(); 
        
        
        WebElement relatedLists = (new WebDriverWait(driver, 40)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.cApplicationForm")));
        assertNotNull(null, relatedLists);
        
        
    }
	
	@After
	public void stopBrowser() {
		EnvironmentManager.shutDownDriver();
	}	
	
	
}
