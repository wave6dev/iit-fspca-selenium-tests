package tests;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import environment.EnvironmentManager;
import environment.RunEnvironment;

public class GuestSubmitProduceCaseTest {


	@Before
	public void startBrowser() {
		EnvironmentManager.initWebDriver();
	}
	
	@Test
    public void SubmitProduceCase() {
    	
    	WebDriver driver = RunEnvironment.getWebDriver();
    	
        driver.get("https://full-fspca.cs25.force.com/FSPCA/s/");
        
       
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".slds-is-relative:nth-child(5) .slds-truncate")));
        element.click();
        
        
        WebElement input1 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-1")));
        WebElement input2 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-2")));
        WebElement input4 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-4")));
        WebElement input6 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-6")));
        WebElement input7 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-7")));
        
        
        input1.sendKeys("TEST");
        input2.sendKeys("Automated TEST");
        input4.sendKeys("SELENIUM");
        input6.sendKeys("scott.morrison@wave6.com");
        input7.sendKeys("613-555-1234");
             
        WebElement checkbox = driver.findElement(By.xpath("//div[@id='caseTermsContainer']/input"));
        checkbox.click();

        
        WebElement submitButton = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='caseSubmitButtonContainer']/button")));        
        submitButton.sendKeys(Keys.ENTER);
                
        (new WebDriverWait(driver, 20)).until(ExpectedConditions.stalenessOf(input1));
        
        WebElement input8 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-8")));
        assertEquals("", input8.getAttribute("value"));

    }
	
	@After
	public void stopBrowser() {
		EnvironmentManager.shutDownDriver();
	}
	

}
