package tests;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import environment.EnvironmentManager;
import environment.RunEnvironment;

public class UserSubmitFSVPLIApplication {

	@Before
	public void startBrowser() {
		EnvironmentManager.initWebDriver();
	}
	
	@Test
    public void SubmitFSVPLIApplication() {
    	
    	WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://full-fspca.cs25.force.com/FSPCA/s/");
        
        WebElement loginLink = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='header-overlay']/div/div[5]/div/div/div/div/a/span")));
        loginLink.click(); 
       
        
        WebElement usernameInput = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#sfdc_username_container .inputBox")));
        WebElement passwordInput = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#sfdc_password_container .inputBox")));
        
        usernameInput.sendKeys("amy.seibert@wave6.com");
        passwordInput.sendKeys("Success123");
        
        WebElement loginButton = driver.findElement(By.cssSelector(".loginButton"));
        loginButton.click(); 
        
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".slds-is-relative:nth-child(6) .slds-truncate")));
        element.click();
        
        WebElement newApplication = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//section/div/button")));
        newApplication.click();
        
        WebElement dialogContainer = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.cLI_Application")));
        assertNotNull(null, dialogContainer);

        
        WebElement fsvpCheckbox = (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.fsvpcheckbox .slds-checkbox--faux")));
        assertNotNull(null, fsvpCheckbox);
        fsvpCheckbox.click();
        
        WebElement trainingExperience = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div/div/textarea")));
        trainingExperience.sendKeys("testing creating an li application");
        
        
        WebElement government = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[7]/div/textarea")));
        government.sendKeys("TEST");

        WebElement international = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[8]/div/textarea")));
        international.sendKeys("TEST");        

        WebElement haccp = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[9]/div/textarea")));
        haccp.sendKeys("TEST");        
        
        WebElement pcha = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[10]/div/textarea")));
        pcha.sendKeys("TEST");  

        WebElement produce = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[11]/div/textarea")));
        produce.sendKeys("TEST");          

        WebElement combination = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[12]/div/textarea")));
        combination.sendKeys("TEST");          

        WebElement academic = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[13]/div/textarea")));
        academic.sendKeys("TEST");           

        WebElement safety = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[14]/div/textarea")));
        safety.sendKeys("TEST");           

        WebElement foodimport = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[15]/div/textarea")));
        foodimport.sendKeys("TEST");             

        WebElement seminar = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[16]/div/textarea")));
        seminar.sendKeys("TEST");         

        WebElement additional = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[17]/div/select")));
        additional.sendKeys("n");
        
        WebElement history = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='commonGS']/div[18]/div/textarea")));
        history.sendKeys("TEST");            
      

        WebElement delivery = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@id='commonGS']/div/div/textarea)[13]")));
        delivery.sendKeys("TEST");   

        WebElement name = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[5]/div/input")));
        name.sendKeys("Amy Seibert");   
        
        
        List<WebElement> allProductsName = driver.findElements(By.cssSelector("select"));
        
        for(WebElement select: allProductsName) {
        	select.sendKeys("5");
        	select.sendKeys(">");
        }
        

        WebElement saveButton = (new WebDriverWait(driver, 30)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[6]/button")));
        saveButton.click(); 
        
        
        
        
        WebElement relatedLists = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.cApplicationForm")));
        assertNotNull(null, relatedLists);
        
        
    }
	
	@After
	public void stopBrowser() {
		EnvironmentManager.shutDownDriver();
	}		
	
	
	
	
}
