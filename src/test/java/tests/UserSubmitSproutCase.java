package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import environment.EnvironmentManager;
import environment.RunEnvironment;

public class UserSubmitSproutCase {

	@Before
	public void startBrowser() {
		EnvironmentManager.initWebDriver();
	}
	
	@Test
    public void SubmitProduceCase() {
    	
    	WebDriver driver = RunEnvironment.getWebDriver();
        driver.get("https://full-fspca.cs25.force.com/FSPCA/s/");
        
        WebElement loginLink = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='header-overlay']/div/div[5]/div/div/div/div/a/span")));
        loginLink.click(); 
       
        
        WebElement usernameInput = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#sfdc_username_container .inputBox")));
        WebElement passwordInput = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#sfdc_password_container .inputBox")));
        
        usernameInput.sendKeys("amy.seibert@wave6.com");
        passwordInput.sendKeys("Success123");
        
        WebElement loginButton = driver.findElement(By.cssSelector(".loginButton"));
        loginButton.click(); 
        
        
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".slds-is-relative:nth-child(5) .slds-truncate")));
        element.click();
        
        
        WebElement input1 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-1")));
        WebElement input2 = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("input-2")));
        
        
        input1.sendKeys("TEST");
        input2.sendKeys("Automated TEST");

             
        WebElement checkbox = driver.findElement(By.xpath("//div[@id='caseTermsContainer']/input"));
        checkbox.click();

        
        WebElement submitButton = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='caseSubmitButtonContainer']/button")));        
        submitButton.sendKeys(Keys.ENTER);
                
        (new WebDriverWait(driver, 20)).until(ExpectedConditions.stalenessOf(input1));
        
    }
	
	@After
	public void stopBrowser() {
		EnvironmentManager.shutDownDriver();
	}
	
	
}
